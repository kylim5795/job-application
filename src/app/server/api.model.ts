export interface Mydb {
  date: Date;
  jobDescription: string;
  //jobLocation: JobLocation;
  companyAddrOne: string;
  companyAddrTwo: string;
  companyCity: string;
  postcode: number;
  jobStatusOn: boolean;
  jobTitle: string;
  poster: string;
  id?: string;
}

export interface JobLocation {
  addrOne: string;
  addrTwo: string;
  city: string;
  postcode: number;
}
