import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Mydb } from "./api.model";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  private messageSource = new BehaviorSubject("false");
  currentMessage = this.messageSource.asObservable();

  constructor(private http: HttpClient) {}

  onFetchGet() {
    //Get data from firebase
    this.fetchGet();
  }

  changeMessage(message: string) {
    this.messageSource.next(message);
  }

  private fetchGet() {
    this.http
      .get<{ [key: string]: Mydb }>(
        "https://job-application-a7b64.firebaseio.com/mydb.json"
      )
      .pipe(
        map(responseData => {
          const dataArray: Mydb[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              console.log("see key:>>>", key);
              dataArray.push({ ...responseData[key], id: key });
            }
          }
          return dataArray;
        })
      )
      .subscribe(data => {
        console.log(data);
      });
  }
}
