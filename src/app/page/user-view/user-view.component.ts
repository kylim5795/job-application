import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import { ApiService } from "../../server/api.service";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

import { Mydb } from "../../server/api.model";

@Component({
  selector: "app-user-view",
  templateUrl: "./user-view.component.html",
  styleUrls: ["./user-view.component.css"]
})
export class UserViewComponent implements OnInit {
  fullData: Mydb[] = [];

  constructor(
    private router: Router,
    private apiService: ApiService,
    private http: HttpClient
  ) {
    //this.fullData = this.apiService.onFetchGet();
    //console.log("show me bla bla bla>>>", this.apiService.onFetchGet());
  }

  ngOnInit() {
    this.fetchGet();
  }

  onSearch() {
    //console.log("onShow()>>>>", this.fullData);
  }

  onFetchGet() {
    this.fetchGet();
  }

  private fetchGet() {
    this.http
      .get<{ [key: string]: Mydb }>(
        "https://job-application-a7b64.firebaseio.com/mydb.json"
      )
      .pipe(
        map(responseData => {
          const dataArray = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              dataArray.push({ ...responseData[key], id: key });
            }
          }
          return dataArray;
        })
      )
      .subscribe(data => {
        //console.log(data);
        this.fullData = data;
      });
  }

  goDetails(data) {
    //console.log("log from b4>>>", data);
    let test: NavigationExtras = {
      queryParams: data
    };
    this.router.navigate(["/jobDetail"], test);
  }
}
