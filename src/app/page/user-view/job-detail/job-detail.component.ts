import { Component, OnInit } from "@angular/core";
import { Mydb } from "src/app/server/api.model";
import { ActivatedRoute, Params } from "@angular/router";

@Component({
  selector: "app-job-detail",
  templateUrl: "./job-detail.component.html",
  styleUrls: ["./job-detail.component.css"]
})
export class JobDetailComponent implements OnInit {
  available = false;
  detailsData: any;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    //this.detailsData = this.route.snapshot.paramMap.get("data");
    //this.detailsData = this.route.snapshot.params["data"];
    this.route.queryParams.subscribe(params => {
      this.detailsData = params;
      //console.log("this is detailsData[]", this.detailsData);
    });
    //console.log(this.route.snapshot.queryParamMap.get("data"));
  }
}
