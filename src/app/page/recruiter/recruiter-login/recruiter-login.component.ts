import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../../../server/api.service";

@Component({
  selector: "app-recruiter-login",
  templateUrl: "./recruiter-login.component.html",
  styleUrls: ["./recruiter-login.component.css"]
})
export class RecruiterLoginComponent implements OnInit {
  isLogin: any;

  constructor(private router: Router, private apiService: ApiService) {}

  ngOnInit() {
    this.apiService.currentMessage.subscribe(
      message => (this.isLogin = message)
    );
  }

  doLogin() {
    this.apiService.changeMessage("true");
    this.router.navigate(["/recHome"]);
    //this.apiService.onFetchGet();
  }

  // doTest() {
  //   this.apiService.onFetchGet();
  // }
}
