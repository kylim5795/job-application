import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

import { Mydb } from "../../../server/api.model";

@Component({
  selector: "app-recruiter-edit",
  templateUrl: "./recruiter-edit.component.html",
  styleUrls: ["./recruiter-edit.component.css"]
})
export class RecruiterEditComponent implements OnInit {
  fullData: Mydb[] = [];

  constructor(private router: Router, private http: HttpClient) {}

  ngOnInit() {
    this.fetchGet();
  }

  doEdit(data) {
    //this.router.navigate(["recPost"]);
    let url =
      "https://job-application-a7b64.firebaseio.com/mydb/" + data.id + "/.json";
    //console.log(url);
    this.updatePatch(url, data.jobStatusOn);
  }

  doDelete(data) {
    //console.log("data deleted", data);
    let url =
      "https://job-application-a7b64.firebaseio.com/mydb/" + data.id + "/.json";
    //console.log(url);
    this.httpDelete(url);
  }

  private updatePatch(url, jobStatus: boolean) {
    this.http
      .patch(url, { date: new Date(), jobStatusOn: !jobStatus })
      .subscribe(
        val => {
          console.log("PATCH call successful value returned in body", val);
        },
        response => {
          console.log("PATCH call in error", response);
        },
        () => {
          console.log("The PATCH observable is now completed.");
          window.location.reload();
        }
      );
  }

  private httpDelete(url) {
    this.http.delete(url).subscribe(
      val => {
        console.log("DELETE call successful value returned in body", val);
      },
      response => {
        console.log("DELETE call in error", response);
      },
      () => {
        console.log("DELETE observable is completed");
        window.location.reload();
      }
    );
  }

  private fetchGet() {
    this.http
      .get<{ [key: string]: Mydb }>(
        "https://job-application-a7b64.firebaseio.com/mydb.json"
      )
      .pipe(
        map(responseData => {
          const dataArray = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              dataArray.push({ ...responseData[key], id: key });
            }
          }
          return dataArray;
        })
      )
      .subscribe(data => {
        //console.log(data);
        this.fullData = data;
      });
  }
}
