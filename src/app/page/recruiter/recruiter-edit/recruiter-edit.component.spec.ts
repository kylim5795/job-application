import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterEditComponent } from './recruiter-edit.component';

describe('RecruiterEditComponent', () => {
  let component: RecruiterEditComponent;
  let fixture: ComponentFixture<RecruiterEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecruiterEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
