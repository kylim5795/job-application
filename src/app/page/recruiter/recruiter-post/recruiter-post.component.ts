import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-recruiter-post",
  templateUrl: "./recruiter-post.component.html",
  styleUrls: ["./recruiter-post.component.css"]
})
export class RecruiterPostComponent implements OnInit {
  jobTitle: string;
  jobDescription: string;
  companyAddrOne: string;
  companyAddrTwo: string;
  companyCity: string;
  postcode: number;
  jobStatusOn: boolean;
  date = new Date();
  poster = "test1@email.com";

  constructor(private router: Router, private http: HttpClient) {}

  ngOnInit() {}

  onCreatePost(postData: {
    jobTitle: string;
    jobDescription: string;
    companyAddrOne: string;
    companyAddrTwo: string;
    companyCity: string;
    postcode: number;
    jobStatusOn: boolean;
    date: Date;
    poster: string;
  }) {
    console.log("postData", postData);
    this.http
      .post<{ name: string }>(
        "https://job-application-a7b64.firebaseio.com/mydb.json",
        postData
      )
      .subscribe(
        responseData => {
          console.log(responseData);
        },
        response => {
          console.log("POST call in error", response);
        },
        () => {
          console.log("The POST observable is now completed.");
          this.doPost();
        }
      );
  }

  doPost() {
    this.router.navigate(["recHome"]);
  }

  goHome() {
    this.router.navigate(["recHome"]);
  }
}
