import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavHeadComponent } from "./nav/nav-head/nav-head.component";
import { UserViewComponent } from "./page/user-view/user-view.component";
import { JobDetailComponent } from "./page/user-view/job-detail/job-detail.component";
import { RecruiterLoginComponent } from "./page/recruiter/recruiter-login/recruiter-login.component";
import { RecruiterHomeComponent } from "./page/recruiter/recruiter-home/recruiter-home.component";
import { RecruiterEditComponent } from "./page/recruiter/recruiter-edit/recruiter-edit.component";
import { RecruiterPostComponent } from "./page/recruiter/recruiter-post/recruiter-post.component";

@NgModule({
  declarations: [
    AppComponent,
    NavHeadComponent,
    UserViewComponent,
    JobDetailComponent,
    RecruiterLoginComponent,
    RecruiterHomeComponent,
    RecruiterEditComponent,
    RecruiterPostComponent
  ],
  imports: [BrowserModule, HttpClientModule, FormsModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
