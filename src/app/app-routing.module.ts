import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UserViewComponent } from "./page/user-view/user-view.component";
import { JobDetailComponent } from "./page/user-view/job-detail/job-detail.component";
import { RecruiterLoginComponent } from "./page/recruiter/recruiter-login/recruiter-login.component";
import { RecruiterHomeComponent } from "./page/recruiter/recruiter-home/recruiter-home.component";
import { RecruiterEditComponent } from "./page/recruiter/recruiter-edit/recruiter-edit.component";
import { RecruiterPostComponent } from "./page/recruiter/recruiter-post/recruiter-post.component";

const routes: Routes = [
  { path: "", component: UserViewComponent },
  { path: "userView", component: UserViewComponent },
  { path: "jobDetail", component: JobDetailComponent },
  { path: "loginPage", component: RecruiterLoginComponent },
  { path: "recHome", component: RecruiterHomeComponent },
  { path: "recEdit", component: RecruiterEditComponent },
  { path: "recPost", component: RecruiterPostComponent }
  //{path: "**", component: UserViewComponent}
];
//test start
// const appRoutes: Routes = [
//   { path: "", component: HomeComponent },
//   {
//     path: "users",
//     component: UsersComponent,
//     children: [{ path: ":id/:name", component: UserComponent }]
//   },
//   {
//     path: "servers",
//     // canActivate: [AuthGuard],
//     canActivateChild: [AuthGuard],
//     component: ServersComponent,
//     children: [
//       {
//         path: ":id",
//         component: ServerComponent,
//         resolve: { server: ServerResolver }
//       },
//       {
//         path: ":id/edit",
//         component: EditServerComponent,
//         canDeactivate: [CanDeactivateGuard]
//       }
//     ]
//   },
//   // { path: 'not-found', component: PageNotFoundComponent },
//   {
//     path: "not-found",
//     component: ErrorPageComponent,
//     data: { message: "Page not found!" }
//   },
//   { path: "**", redirectTo: "/not-found" }
// ];

//test end

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
