import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../server/api.service";

@Component({
  selector: "nav-head",
  templateUrl: "./nav-head.component.html",
  styleUrls: ["./nav-head.component.css"]
})
export class NavHeadComponent implements OnInit {
  isLogin: any;

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.apiService.currentMessage.subscribe(
      message => (this.isLogin = message)
    );
  }

  doLogout() {
    this.apiService.changeMessage("false");
  }
}
